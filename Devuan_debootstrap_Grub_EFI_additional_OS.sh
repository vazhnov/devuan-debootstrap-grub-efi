#!/usr/bin/env bash

# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2018 Quentin Dufour
# SPDX-FileCopyrightText: 2023-2024 Alexey Vazhnov

# Based on https://gist.github.com/superboum/1c7adcd967d3e15dfbd30d04b9ae6144
# shellcheck disable=SC2317  # Because there is `exit` command in the middle of the code: "Command appears to be unreachable. Check usage (or ignore if invoked indirectly)."

set -o nounset
set -o errexit
set -o pipefail
shopt -s dotglob

usage()
{
  cat << EOF
Usage: $0 /dev/sdXY /dev/sdXN new_hostname
ARGUMENTS:
  1st - partition for /boot/efi, usually formatted FAT32
  2nd - partition for / (root), usually formatted into ext4
  3rd - hostname of new OS

Partitions should exist and already formatted into proper FS (vfat for BOOT and something like EXT4 for root).

Difference with 'Devuan_debootstrap_Grub_EFI.sh':
* Do not create partitons
* Do not format filesystems
* Create EFI boot entry

First edit the script and adjust all the places marked with "FIXME".
EOF
}

# FIXME: set 3 variables below
PARTITION_PREFIX="1TB_SSD"  # Maximum 31 symbols: because partition name field is 72 bytes = 36 UTF-16LE code units, + suffix "_ROOT"
RELEASE_NAME="excalibur"
NEWUSER="myuser"

ROOTFS="/mnt/installing-rootfs"
DEVICE_PARTITIONS_PATH_EFI="$1"
[ -z "${DEVICE_PARTITIONS_PATH_EFI}" ] && usage && exit 1
DEVICE_PARTITIONS_PATH_ROOT="$2"
[ -z "${DEVICE_PARTITIONS_PATH_ROOT}" ] && usage && exit 1
NEW_HOSTNAME=$3
[ -z "${NEW_HOSTNAME}" ] && usage && exit 1

echo "Selected partitions are:"
echo "EFI: $DEVICE_PARTITIONS_PATH_EFI"
echo "Root: $DEVICE_PARTITIONS_PATH_ROOT"
echo "This script is DANGEROUS!"
echo "It will DESTROY information at selected partitions!"
read -r -p "[Press enter to continue or CTRL+C to stop]"

# FIXME: comment out ONLY when adjusted **all** "FIXME":
usage; exit 0

echo "Mount OS partition:"
mkdir -pv "${ROOTFS}"
mount -v "$DEVICE_PARTITIONS_PATH_ROOT" "${ROOTFS}"  # "${PARTITION_PREFIX}_ROOT"

echo "Debootstrap system:"
debootstrap --variant=minbase --arch amd64 --include=iproute2,curl,wget,devuan-keyring,zsh,sudo,locales,ca-certificates,zstd "${RELEASE_NAME}" "${ROOTFS}" http://pl.deb.devuan.org/merged  # Installs `sysv-rc` by default

echo "Mount EFI partition:"
mkdir -pv "${ROOTFS}/boot/efi"
mount -v "$DEVICE_PARTITIONS_PATH_EFI" "${ROOTFS}/boot/efi"  # "${PARTITION_PREFIX}_EFI"

cat <<EOF > "${ROOTFS}/etc/apt/sources.list"
deb http://pl.deb.devuan.org/merged ${RELEASE_NAME} main contrib non-free non-free-firmware
# deb http://pl.deb.devuan.org/merged ${RELEASE_NAME}-updates main contrib non-free non-free-firmware
# deb http://pl.deb.devuan.org/merged ${RELEASE_NAME}-security main contrib non-free non-free-firmware
# deb http://pl.deb.devuan.org/merged ${RELEASE_NAME}-backports main contrib non-free non-free-firmware
EOF

# FIXME: adjust shell and groups if needed
useradd --root "${ROOTFS}" --create-home --groups sudo,adm --shell /bin/zsh "${NEWUSER}"
# By some reason, `passwd` gives errors when executed in chroot:
# > New password: Password change has been aborted.
# > passwd: Authentication token manipulation error
# so here `--root` is using:
echo "Set new password for user ${NEWUSER}:"
passwd --root "${ROOTFS}" "${NEWUSER}"

# Run script with customizations if exist
test -e ./Local_changes_chroot.sh && cp -v ./Local_changes_chroot.sh "${ROOTFS}/"

echo "Get ready for chroot:"
mount --bind /dev "${ROOTFS}/dev"
mount -t devpts /dev/pts "${ROOTFS}/dev/pts"
mount -t proc proc "${ROOTFS}/proc"
mount -t sysfs sysfs "${ROOTFS}/sys"
mount -t tmpfs tmpfs "${ROOTFS}/tmp"
mount --rbind /sys/firmware/efi/efivars "${ROOTFS}/sys/firmware/efi/efivars"

# FIXME:
echo "Creating custom 'GRUB_DISTRIBUTOR' name - use it if you already have any Debian-based OS on this computer to avoid '/boot/efi/EFI/debian' be rewritten by this setup."
mkdir -pv "${ROOTFS}/etc/default/grub.d"
cat <<EOF > "${ROOTFS}/etc/default/grub.d/local.cfg"
GRUB_DISTRIBUTOR="My-Devuan"
EOF

echo "Entering chroot, installing Linux kernel and Grub:"
LANG=C.UTF-8 chroot "${ROOTFS}" bash -x <<'EOF'
  set -o nounset
  set -o errexit
  set -o pipefail
  shopt -s dotglob

  export HOME=/root
  export DEBIAN_FRONTEND=noninteractive

  debconf-set-selections <<< "grub-efi-amd64 grub2/update_nvram boolean false"
  apt-get remove -y -V grub-efi grub-efi-amd64
  apt-get update
  # apt-get install -y -V linux-image-amd64 linux-headers-amd64 grub-efi  # After this operation, 813 MB of additional disk space will be used.
  apt-get install -y -V --no-install-recommends linux-image-amd64 linux-headers-amd64 grub-efi  # After this operation, 404 MB of additional disk space will be used.
  # TODO: maybe `--bootloader-id=debian` should be removed from here and `GRUB_DISTRIBUTOR` from `/etc/default/grub.d` used:
  grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=debian --recheck --no-nvram --removable
  update-grub

  apt-get install -y -V ifupdown acpid
  apt-get install -y -V --no-install-recommends rsyslog cron cron-daemon-common logrotate
  # FIXME: comment out for virtual machines:
  # apt-get install -y -V firmware-linux firmware-realtek firmware-iwlwifi  # After this operation, 251 MB of additional disk space will be used.

  apt-get install -y -V dialog console-setup tzdata
  export DEBIAN_FRONTEND=dialog

  echo "Run ./Local_changes_chroot.sh if exist:"
  test -e /Local_changes_chroot.sh && bash -x /Local_changes_chroot.sh

  dpkg-reconfigure tzdata
  dpkg-reconfigure locales
  dpkg-reconfigure keyboard-configuration

  # FIXME: Install OpenRC (and remove `sysv-rc`). "Always install the new version without prompting":
  # apt-get install -o Dpkg::Options::="--force-confnew" -y -V --no-install-recommends elogind libpam-elogind openrc orphan-sysvinit-scripts
EOF

# FIXME: adjust FS types and partition names
echo "PARTLABEL=\"${PARTITION_PREFIX}_ROOT\"	/		ext4	errors=remount-ro	0  1" >> "${ROOTFS}/etc/fstab"
echo "PARTLABEL=\"${PARTITION_PREFIX}_EFI\"	/boot/efi	vfat	umask=0077		0  1" >> "${ROOTFS}/etc/fstab"

cat <<EOF > "${ROOTFS}/etc/sudoers.d/${NEWUSER}"
${NEWUSER}   ALL=(ALL) NOPASSWD: ALL
# ${NEWUSER}   ALL=(ALL) NOPASSWD: /usr/bin/apt, /usr/bin/apt-get
Defaults:${NEWUSER} env_keep+=HOME
EOF
chmod -v 0440 "${ROOTFS}/etc/sudoers.d/${NEWUSER}"

echo "${NEW_HOSTNAME}" > "${ROOTFS}/etc/hostname"

# Based on https://www.debian.org/releases/stable/amd64/apds03.en.html , "D.3.4.4. Configure networking":
cat <<EOF > "${ROOTFS}/etc/hosts"
127.0.0.1 localhost
127.0.1.1 ${NEW_HOSTNAME}

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
EOF

echo "** Setup is finished **"
echo "The last step is to unmount chroot partitions from ${ROOTFS}."
echo "You can cancel it to do some other changes manually."
read -r -p "[Press enter to unmount partitions or CTRL+C to cancel the last step]"

echo "Unmounting filesystems:"
umount -v --recursive "${ROOTFS}"

echo "Done"
