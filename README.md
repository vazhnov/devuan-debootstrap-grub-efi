Setup [Devuan GNU/Linux](https://devuan.org) with EFI boot partition to HDD/SSD/NVME/USB/SD card or into a file image.

The code is based on https://gist.github.com/superboum/1c7adcd967d3e15dfbd30d04b9ae6144

## Table of contents

<!--
 Generate with: `md_toc --in-place gitlab README.md` (package `python3-md-toc` is needed).
 Also, in GitLab, it can be replaced with just `[[_TOC_]]`.
-->
<!--TOC-->

- [Table of contents](#table-of-contents)
- [Partitions](#partitions)
- [Prerequisites](#prerequisites)
- [How to use the script](#how-to-use-the-script)
  - [How to chroot manually](#how-to-chroot-manually)
  - [How to test the script on a file image](#how-to-test-the-script-on-a-file-image)
- [Questions and answers](#questions-and-answers)
- [See also](#see-also)
- [Copyright](#copyright)

<!--TOC-->

## Partitions

Script works with a whole empty drive. No partitions should be created in advance.

## Prerequisites

```sh
sudo apt update
sudo apt-get -V install debootstrap
```

If you use Devuan — no more steps needed.

If you use Debian/Ubuntu:
* The simplest way is to download the latest packages `debootstrap`, `devuan-keyring`, `distro-info-data` from https://pkgmaster.devuan.org/devuan/pool/main/d/ and install them with `dpkg -i /path/to/*.deb`.
* Or, try to copy some standalone files from Devuan packages:
  + Copy https://gitea.devuan.org/devuan/debootstrap/src/branch/suites/unstable/scripts/ceres to `/usr/share/debootstrap/scripts/ceres` (I've never tested).
  + File `/usr/share/keyrings/devuan-archive-keyring.gpg` from package `devuan-keyring` is needed, you can install some fresh `.deb` file from https://pkgmaster.devuan.org/devuan/pool/main/d/devuan-keyring/
  + `distro-info-data` — ?

## How to use the script

1. Edit the script: find all `FIXME` in the script code, edit them as you need.
2. If customizations are needed, copy `Local_changes_chroot.sh.example` into `Local_changes_chroot.sh` and edit it.
3. Find path to the target disk, for example, with `lsblk`.
4. Run `sudo ./Devuan_debootstrap_Grub_EFI.sh /dev/sdX`
5. Answer questions (yes, some steps are interactive).
6. Wait for response "Done".

### How to chroot manually

If you need to install additional packages, or for example create an user account, you can execute shell into your fresh installed system (run with `root` user):

```sh
blkid | sort  # to find partitions and their labels
ROOTFS="/mnt/installing-rootfs"
mount PARTLABEL="1TB_SSD_ROOT" "$ROOTFS"
mount PARTLABEL="1TB_SSD_EFI"  "$ROOTFS/boot/efi"
mount --bind /dev ${ROOTFS}/dev
mount -t devpts /dev/pts ${ROOTFS}/dev/pts
mount -t proc proc ${ROOTFS}/proc
mount -t sysfs sysfs ${ROOTFS}/sys
mount -t tmpfs tmpfs ${ROOTFS}/tmp
LANG=C.UTF-8 chroot ${ROOTFS} /bin/bash
```

At the end:

```sh
exit
umount -v --recursive "${ROOTFS}"
```

### How to test the script on a file image

Here is an example how to create an `.img` file (I use [zram](https://en.wikipedia.org/wiki/Zram) disk for it), then mount it into a [loop device](https://en.wikipedia.org/wiki/Loop_device), and then run the script against the loop device.

P.S.: it looks better to work _directly_ with zram device instead of creating a FS on it and then creating a file image, but I couldn't figure out how to work with partitions in zram.

```sh
sudo -s
modprobe zram
zram_device=$(zramctl -f -a zstd -s 16G)
mkfs.ext4 -q -E discard -L "zram_16G" "$zram_device"
mkdir -p /mnt/zram
mount "$zram_device" /mnt/zram
dd if=/dev/zero of=/mnt/zram/disk.img bs=1024k seek=10240 count=0   # Create 10GB disk image
chown -R "$SUDO_USER" /mnt/zram
loop_device=$(losetup --show --find --partscan /mnt/zram/disk.img)  # or you can use `kpartx` for it
bash ./Devuan_debootstrap_Grub_EFI.sh "$loop_device" host1
losetup --detach "$loop_device"
```

You can check memory spend for the disk, here is real example (1.4GB image data, 714MB RAM spent):

```console
 # zramctl
NAME       ALGORITHM DISKSIZE  DATA  COMPR  TOTAL STREAMS MOUNTPOINT
/dev/zram0 zstd           16G  1.4G 704.2M 714.8M       8 /mnt/zram
```

Run in QEMU:

Packages needed: `qemu-system-x86 ovmf`. Also `qemu-system-gui` if you are planning to run virtual machine with GUI.

```sh
sudo apt-get -V install qemu-system-x86 qemu-system-gui ovmf
```

Next command can be executed without `root` permissions. It starts QEMU virtual machine with EFI support:

```sh
qemu-system-x86_64 -enable-kvm -m 2048 -net user -drive format=raw,if=virtio,file="/mnt/zram/disk.img" -bios /usr/share/qemu/OVMF.fd -vga qxl
```

Or, you can convert the RAW image into [VirtualBox](https://virtualbox.org) format and run it:

```sh
VBOXNAME="Devuan_test_zram"
VBoxManage convertfromraw /mnt/zram/disk.img /mnt/zram/disk.vdi --format VDI
VBoxManage createvm --name=$VBOXNAME --basefolder=/mnt/zram --default --ostype=Debian_64 --register
VBoxManage modifyvm $VBOXNAME --firmware=efi64
VBoxManage storageattach $VBOXNAME --storagectl "SATA" --port 0 --device 0 --type hdd --medium /mnt/zram/disk.vdi
VBoxManage startvm $VBOXNAME
VBoxManage unregistervm $VBOXNAME --delete
```

Cleanup:

```sh
umount /dev/zram0
zramctl -r /dev/zram0
```

## Questions and answers

**Q: Why multiple scripts instead of one universal script?**

**A:** Because every script is simple. One big universal script is complicated to: create, read and support.

**Q: How to run script(s) fully autonomous, without interactive steps?**

**A:** Remove in the script all lines:
* with `dpkg-reconfigure`
* with `read -r -p`
* with `passwd --root`


**Q: How to create an image with OpenRC init system?**

**A:** By default, Devuan's `debootstrap` install OS with `sysv-rc` package.
I couldn't achieve choosing another init system with `debootstrap` arguments, so there is a separate step for this in the script.
Uncomment the line:
```sh
apt-get install -o Dpkg::Options::="--force-confnew" -y -V --no-install-recommends elogind libpam-elogind openrc orphan-sysvinit-scripts
```

**Q: How to install an additional system to a drive**

**A:** Prepare partition(s), then use `Devuan_debootstrap_Grub_EFI_additional_OS.sh`.

## See also

* [D.3. Installing Debian GNU/Linux from a Unix/Linux system](https://www.debian.org/releases/stable/amd64/apds03.en.html) — official documentation from Debian.
* https://gitlab.com/vazhnov/proxmox-devuan-containers — LXC containers (without bootloaders) for Proxmox VE.
* https://gitlab.com/vazhnov/devuan-docker-images — build container images with GitLab CI/CD pipelines.
* https://gitlab.com/vazhnov/vmdb2-examples — how to install Debian-based distro with [vmdb2](https://vmdb2.liw.fi).
* [Devuan GNU+Linux command-line installation guide](https://dev1galaxy.org/viewtopic.php?id=4919) at the official Devuan forum.

## Copyright

MIT license.

The code is based on https://gist.github.com/superboum/1c7adcd967d3e15dfbd30d04b9ae6144, which is published under MIT license.
