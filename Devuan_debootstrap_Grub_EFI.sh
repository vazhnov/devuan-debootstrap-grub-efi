#!/usr/bin/env bash

# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2018 Quentin Dufour
# SPDX-FileCopyrightText: 2023-2024 Alexey Vazhnov

# Based on https://gist.github.com/superboum/1c7adcd967d3e15dfbd30d04b9ae6144
# shellcheck disable=SC2317  # Because there is `exit` command in the middle of the code: "Command appears to be unreachable. Check usage (or ignore if invoked indirectly)."

set -o nounset
set -o errexit
set -o pipefail
shopt -s dotglob

# FIXME
PARTITION_PREFIX="1TB_SSD"  # Maximum 31 symbols: because partition name field is 72 bytes = 36 UTF-16LE code units, + suffix "_ROOT"
RELEASE_NAME="excalibur"
NEWUSER="myuser"
ROOTFS="/mnt/installing-rootfs"

# Partition name should be not more than 7 symbols because of error: "mkfs.vfat: Label can be no longer than 11 characters" for "${PARTITION_PREFIX}_EFI"
# Using Bash substring expansion, `${parameter:offset:length}` to create a new variable **only for mkfs.vfat**:
PARTITION_PREFIX_SHORT="${PARTITION_PREFIX:0:7}"

DEVICE=$1
[ -z "${DEVICE}" ] && echo "Usage: $0 /dev/sdX new_hostname" && exit 1

NEW_HOSTNAME=$2
[ -z "${NEW_HOSTNAME}" ] && echo "Usage: $0 /dev/sdX new_hostname" && exit 1

# **NVME workaround**: a dedicated variable is needed because in GNU/Linux NVME drives has "p" prefix for partitions:
# % lsblk
# NAME           MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINTS
# sda              8:0    0 894.3G  0 disk
# ├─sda1           8:1    0    10M  0 part
# ├─sda2           8:2    0   954M  0 part  /boot/efi
# ├─sda3           8:3    0   954M  0 part  /boot
# └─sda4           8:4    0 186.3G  0 part
#   └─sda4_crypt 254:0    0 186.2G  0 crypt /
# nvme0n1        259:0    0 238.5G  0 disk
# ├─nvme0n1p1    259:1    0    11M  0 part
# ├─nvme0n1p2    259:2    0   488M  0 part
# └─nvme0n1p3    259:3    0  71.1G  0 part
if [[ "${DEVICE}" =~ nvme|loop ]] ; then
  echo "NVME/loop drive detected."
  DEVICE_PARTITIONS="${DEVICE}p"
else
  DEVICE_PARTITIONS="${DEVICE}"
fi

udevadm info -n "${DEVICE}" -q property
echo "Selected device is ${DEVICE}."
echo "This script is DANGEROUS!"
echo "It will DESTROY information at drive ${DEVICE}!"
read -r -p "[Press enter to continue or CTRL+C to stop]"

# FIXME: comment out ONLY when adjusted **all** "FIXME":
usage; exit 0

echo "Umount ${DEVICE}:"
umount -v --recursive "${DEVICE}" || true  # ignore error "not mounted"

echo "Set partition table to GPT (UEFI):"
parted "${DEVICE}" --align optimal --script mktable gpt

echo "Create BIOS GRUB partition for compatibility:"
parted "${DEVICE}" --align optimal --script mkpart "${PARTITION_PREFIX}_BIOS_GRUB" 1MiB 12MiB  # Recommended size is 10MB, I decided to skip 1MB and align the end to 4MB. So `0MiB 10MiB` should work here.
parted "${DEVICE}" --align optimal --script set 1 bios_grub on  # For compatibility GPT with non-EFI BIOS
echo "Create EFI partition:"
parted "${DEVICE}" --align optimal --script mkpart "${PARTITION_PREFIX}_EFI" fat32 12MiB 500MiB
parted "${DEVICE}" --align optimal --script set 2 msftdata on

echo "Create OS partition:"
# FIXME: label, size and FS type
parted "${DEVICE}" --align optimal --script mkpart "${PARTITION_PREFIX}_ROOT" ext4 500MiB 30%

if [[ ! "${DEVICE}" =~ loop|zram ]] ; then
  # fails on loop and zram devices: "blockdev: ioctl error on BLKRRPART: Invalid argument"
  blockdev --rereadpt "${DEVICE}"
  # Pause to avoid error
  # > mkfs.vfat: unable to open /dev/sda1: No such file or directory
  sleep 1
fi

echo "Format partitions:"
# FIXME: change FS type if needed
mkfs.vfat -n "${PARTITION_PREFIX_SHORT}_EFI" "${DEVICE_PARTITIONS}2"
mkfs.ext4 -L "${PARTITION_PREFIX}_ROOT" -E discard "${DEVICE_PARTITIONS}3"

echo "Mount OS partition:"
mkdir -pv "${ROOTFS}"
mount -v "${DEVICE_PARTITIONS}3" "${ROOTFS}"  # "${PARTITION_PREFIX}_ROOT"

echo "Debootstrap system:"
debootstrap --variant=minbase --arch amd64 --include=iproute2,curl,wget,devuan-keyring,zsh,sudo,locales,ca-certificates,zstd "${RELEASE_NAME}" "${ROOTFS}" http://pl.deb.devuan.org/merged  # Installs `sysv-rc` by default

echo "Mount EFI partition:"
mkdir -pv "${ROOTFS}/boot/efi"
mount -v "${DEVICE_PARTITIONS}2" "${ROOTFS}/boot/efi"  # "${PARTITION_PREFIX}_EFI"

cat <<EOF > "${ROOTFS}/etc/apt/sources.list"
deb http://pl.deb.devuan.org/merged ${RELEASE_NAME} main contrib non-free non-free-firmware
# deb http://pl.deb.devuan.org/merged ${RELEASE_NAME}-updates main contrib non-free non-free-firmware
# deb http://pl.deb.devuan.org/merged ${RELEASE_NAME}-security main contrib non-free non-free-firmware
# deb http://pl.deb.devuan.org/merged ${RELEASE_NAME}-backports main contrib non-free non-free-firmware
EOF

# FIXME: adjust shell and groups if needed
useradd --root "${ROOTFS}" --create-home --groups sudo,adm --shell /bin/zsh "${NEWUSER}"
# By some reason, `passwd` gives errors when executed in chroot:
# > New password: Password change has been aborted.
# > passwd: Authentication token manipulation error
# so here `--root` is using:
echo "Set new password for user ${NEWUSER}:"
passwd --root "${ROOTFS}" "${NEWUSER}"

# Run script with customizations if exist
test -e ./Local_changes_chroot.sh && cp -v ./Local_changes_chroot.sh "${ROOTFS}/"

echo "Get ready for chroot:"
mount --bind /dev "${ROOTFS}/dev"
mount -t devpts /dev/pts "${ROOTFS}/dev/pts"
mount -t proc proc "${ROOTFS}/proc"
mount -t sysfs sysfs "${ROOTFS}/sys"
mount -t tmpfs tmpfs "${ROOTFS}/tmp"
# No need to mount EFI partition to avoid adding a boot record into the host BIOS.
# mount --rbind /sys/firmware/efi/efivars "${ROOTFS}/sys/firmware/efi/efivars"

echo "Entering chroot, installing Linux kernel and Grub:"
LANG=C.UTF-8 chroot "${ROOTFS}" bash -x <<'EOF'
  set -o nounset
  set -o errexit
  set -o pipefail
  shopt -s dotglob

  export HOME=/root
  export DEBIAN_FRONTEND=noninteractive

  debconf-set-selections <<< "grub-efi-amd64 grub2/update_nvram boolean false"
  apt-get remove -y -V grub-efi grub-efi-amd64
  apt-get update
  # apt-get install -y -V linux-image-amd64 linux-headers-amd64 grub-efi  # After this operation, 813 MB of additional disk space will be used.
  apt-get install -y -V --no-install-recommends linux-image-amd64 linux-headers-amd64 grub-efi  # After this operation, 404 MB of additional disk space will be used.
  grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=debian --recheck --no-nvram --removable
  update-grub

  apt-get install -y -V ifupdown acpid
  apt-get install -y -V --no-install-recommends rsyslog cron cron-daemon-common logrotate
  # FIXME: comment out for virtual machines:
  # apt-get install -y -V firmware-linux firmware-realtek firmware-iwlwifi  # After this operation, 251 MB of additional disk space will be used.

  apt-get install -y -V dialog console-setup tzdata
  export DEBIAN_FRONTEND=dialog

  echo "Run ./Local_changes_chroot.sh if exist:"
  test -e /Local_changes_chroot.sh && bash -x /Local_changes_chroot.sh

  dpkg-reconfigure tzdata
  dpkg-reconfigure locales
  dpkg-reconfigure keyboard-configuration

  # FIXME: Install OpenRC (and remove `sysv-rc`). "Always install the new version without prompting":
  # apt-get install -o Dpkg::Options::="--force-confnew" -y -V --no-install-recommends elogind libpam-elogind openrc orphan-sysvinit-scripts
EOF

# FIXME: adjust FS types and partition names
echo "PARTLABEL=\"${PARTITION_PREFIX}_ROOT\"	/		ext4	errors=remount-ro	0  1" >> "${ROOTFS}/etc/fstab"
echo "PARTLABEL=\"${PARTITION_PREFIX}_EFI\"	/boot/efi	vfat	umask=0077		0  1" >> "${ROOTFS}/etc/fstab"

cat <<EOF > "${ROOTFS}/etc/sudoers.d/${NEWUSER}"
${NEWUSER}   ALL=(ALL) NOPASSWD: ALL
# ${NEWUSER}   ALL=(ALL) NOPASSWD: /usr/bin/apt, /usr/bin/apt-get
Defaults:${NEWUSER} env_keep+=HOME
EOF
chmod -v 0440 "${ROOTFS}/etc/sudoers.d/${NEWUSER}"

echo "${NEW_HOSTNAME}" > "${ROOTFS}/etc/hostname"

# Based on https://www.debian.org/releases/stable/amd64/apds03.en.html , "D.3.4.4. Configure networking":
cat <<EOF > "${ROOTFS}/etc/hosts"
127.0.0.1 localhost
127.0.1.1 ${NEW_HOSTNAME}

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
EOF

echo "** Setup is finished **"
echo "The last step is to unmount chroot partitions from ${ROOTFS}."
echo "You can cancel it to do some other changes manually."
read -r -p "[Press enter to unmount partitions or CTRL+C to cancel the last step]"

echo "Unmounting filesystems:"
umount -v --recursive "${ROOTFS}"

echo "Done"
